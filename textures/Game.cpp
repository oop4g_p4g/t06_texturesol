#include "Game.h"
#include "WindowUtils.h"
#include "CommonStates.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;



Game::Game(MyD3D& d3d)
	: mPMode(d3d), mD3D(d3d), mpSB(nullptr)
{
	mpSB = new SpriteBatch(&mD3D.GetDeviceCtx());
}


//any memory or resources we made need releasing at the end
void Game::Release()
{
	delete mpSB;
	mpSB = nullptr;
}

//called over and over, use it to update game logic
void Game::Update(float dTime)
{
	switch (state)
	{
	case State::PLAY:
		mPMode.Update(dTime);
	}
}

//called over and over, use it to render things
void Game::Render(float dTime)
{
	mD3D.BeginRender(Colours::Black);


	CommonStates dxstate(&mD3D.GetDevice());
	mpSB->Begin(SpriteSortMode_Deferred, dxstate.NonPremultiplied(), &mD3D.GetSampler(true));

	switch (state)
	{
	case State::PLAY:
		mPMode.Render(dTime, *mpSB);
	}

	mpSB->End();

	mD3D.EndRender();
}


PlayMode::PlayMode(MyD3D & d3d)
	:mD3D(d3d)
{
}

void PlayMode::Update(float dTime)
{
}

void PlayMode::Render(float dTime, DirectX::SpriteBatch & batch) 
{
	//hack mode engaged
	
	//scrolling background
	Sprite spr(mD3D);
	spr.SetTex(*mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "backgroundlayers/InTheRightOrderTheyLookLikeThis.dds", "bgnd", true));
	spr.SetScale(Vector2(1, 1));
	const TexCache::Data& d = spr.GetTexData();
	float scroll = GetClock()*100;
	RECTF rect{scroll,0,d.dim.x+scroll, d.dim.y};
	spr.SetTexRect(rect);
	spr.Draw(batch);

	//normal cat
	spr.SetTex(*mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "cat.dds", "cat", true));
	spr.mPos = Vector2(20, 20);
	spr.SetScale(Vector2(0.125f, 0.125f));
	spr.Draw(batch);

	//3x3 cat magnification
	rect = RECTF{ -512,-512, 1024, 1024 };
	spr.SetTexRect(rect);
	spr.mPos = Vector2(100, 20);
	spr.Draw(batch);

	//vertical cat scrolling
	scroll *= 5.f;
	rect = RECTF{ 0,-scroll, 512, 512-scroll };
	spr.SetScale(Vector2(0.25f, 0.25f));
	spr.SetTexRect(rect);
	spr.mPos = Vector2(300, 20);
	spr.Draw(batch);

}



